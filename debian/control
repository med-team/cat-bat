Source: cat-bat
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>, Nilesh Patra <nilesh@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               diamond-aligner
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/cat-bat
Vcs-Git: https://salsa.debian.org/med-team/cat-bat.git
Homepage: https://github.com/dutilh/CAT
Rules-Requires-Root: no

Package: cat-bat
Architecture: any
Depends: ${python3:Depends},
         ${misc:Depends},
         diamond-aligner,
         prodigal
Description: taxonomic classification of contigs and metagenome-assembled genomes (MAGs)
 Contig Annotation Tool (CAT) and Bin Annotation Tool (BAT) are pipelines
 for the taxonomic classification of long DNA sequences and metagenome
 assembled genomes (MAGs/bins) of both known and (highly) unknown
 microorganisms, as generated by contemporary metagenomics studies. The
 core algorithm of both programs involves gene calling, mapping of
 predicted ORFs against the nr protein database, and voting-based
 classification of the entire contig / MAG based on classification of the
 individual ORFs. CAT and BAT can be run from intermediate steps if files
 are formatted appropriately.
